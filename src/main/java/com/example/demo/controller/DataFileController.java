package com.example.demo.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.ws.http.HTTPBinding;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.TableMetadata;
import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.example.demo.vo.DataVo;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

@RestController
public class DataFileController {
	
	private static Cluster cluster = Cluster.builder().addContactPoint("127.0.0.1").withPort(9042).
			withoutJMXReporting()
			.build();;
	private static Session session = cluster.connect("dev");
	@RequestMapping(method = RequestMethod.GET, value = "/get")
	@ResponseBody
	public String get() throws IOException, Exception {
		List<Insert> batchStatement = new ArrayList<>(1000000);
		long startTime = System.nanoTime();
		for(int i=1000001; i<=1999999;i++) {
		StringBuilder result = new StringBuilder();
		URL url = new URL("http://localhost:3000/api/v1/influencers/" + i);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		urlConnection.setRequestMethod("GET");
		BufferedReader rd = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		String line;
	      while ((line = rd.readLine()) != null) {
	         result.append(line);
	      }
	      rd.close();
	      JSONObject json = new JSONObject(result.toString());
	      long uploadTime = Instant.now().toEpochMilli();
	      Insert insert = QueryBuilder.insertInto("mock_instagram").value("id", Integer.parseInt(json.getString("pk")))
	    		  .value("timestamp", (double) uploadTime)
	    		  .value("name", json.getString("username"))
	    		  .value("followers", Integer.parseInt(json.getString("followerCount")))
	    		  .value("following", Integer.parseInt(json.getString("followingCount")));
	        batchStatement.add(insert);
	      
	      
		}
//		BatchStatement batch = new BatchStatement();
//		for(Insert state : batchStatement) {
//			if(batch.size() < 50000) {
//				batch.add(state);
//			} else {
//				session.execute(batch);
//			batch.clear();
//			}
//			
//		    	
//		}
		long midTime =System.nanoTime();
		batchStatement.stream().parallel().forEach(insert -> {
			session.executeAsync(insert);
		});
		
		long endTime = System.nanoTime();
	    return String.valueOf(midTime - startTime) + " " +String.valueOf(endTime - startTime);
	}
	
	
}
